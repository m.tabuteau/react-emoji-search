import React from 'react';
import './EmojiListItem.css';

export default function EmojiListItem (props) {
  return (
    <li className='emoji'>
      <img src={props.url} alt={props.name} title={props.name} />
      <hr />
    </li>);
}
