import React from 'react';
import EmojiListItem from './EmojiListItem';
import './EmojiList.css';

export default function EmojiList (props) {
  return (
    <ul className='emoji-list'>{
      props.items.map(item =>
        <EmojiListItem key={item.name} name={item.name} url={item.url} />)
    }</ul>
  );
}
