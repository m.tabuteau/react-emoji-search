/* global fetch, localStorage */
const emojiApiUrl = 'https://api.github.com/emojis';

export default async function GetEmojis () {
  let emojis = GetEmojisFromStorage();
  if (emojis) return emojis;

  const response = await fetch(emojiApiUrl);
  const json = await response.json();

  emojis = GetEmojisFromResponseJson(json);
  SetEmojisToStorage(emojis);

  return emojis;
}

function GetEmojisFromStorage () {
  return JSON.parse(localStorage.getItem('emojis'));
}

function SetEmojisToStorage (emojis) {
  localStorage.setItem('emojis', JSON.stringify(emojis));
}

function GetEmojisFromResponseJson (response) {
  const emojis = [];
  for (let name in response) {
    emojis.push({
      name,
      url: response[name]
    });
  }

  return emojis;
}
