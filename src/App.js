import React, { Component } from 'react';
import { BeatLoader } from 'react-spinners';
import EmojiList from './EmojiList';
import GetEmojis from './EmojiService';
import './App.css';

class App extends Component {
  constructor (props) {
    super(props);
    this.nbMaxEmojiShown = 20;
    this.state = {
      emojis: null,
      shownEmojis: null
    };
  }

  async componentDidMount () {
    const emojis = await GetEmojis();
    const shownEmojis = emojis.slice(0, this.nbMaxEmojiShown);
    this.setState({ emojis, shownEmojis });
  }

  searchEmoji (text) {
    const foundEmojis = this.state.emojis.filter(e => e.name.indexOf(text) > -1);
    this.setState({ shownEmojis: foundEmojis.slice(0, this.nbMaxEmojiShown) });
  }

  render () {
    let emojiList = <BeatLoader />;
    if (this.state.emojis) {
      emojiList = <EmojiList items={this.state.shownEmojis} />;
    }

    return (
      <div className='grid-container'>
        <div className='grid-header'>
          <input className='search-input' type='text' onChange={(e) => this.searchEmoji(e.target.value)} />
        </div>
        <div className='grid-body'>
          {emojiList}
        </div>
      </div>
    );
  }
}

export default App;
